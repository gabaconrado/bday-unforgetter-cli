# Birthday Unforgetter CLI

> Command line interface to interact with the [Birthday Unforgetter](https://gitlab.com/gabaconrado/bday-unforgetter)

## Requirements

- In order to run the software is necessary to have [curl](https://curl.se/) installed. The
software was developed against the version `7.68.0`.

## Building

```bash
make build
```

## Running linters

```bash
make lint
```

## Running tests

```bash
make test
```

## Generate documentation

```bash
make docs
```

## Installation

It is necessary to have a [rust](https://www.rust-lang.org/) compiler. The software was developed
against the version `1.55`.

```bash
make install
```

This command will build the binary and install it in `$HOME/.local/bin`, it will error out if the
directory does not exist.

## Usage

To check the CLI help:

```bash
bday-cli --help
```

### Setup

Set the relevant environment variables:

```bash
export BDAY_CLI_USER_PK=<bday-unforgetter-user-id>
export BDAY_CLI_API_KEY=<bday-unforgetter-api-token>
export BDAY_CLI_API_URL=<bday-unforgetter-url>
```

### Basics

To get all birthdays:

```bash
bday-cli birthday get

$ Birthdays:
$   - gaba 1: 1990-01-01; <bday-pk>
$   - gaba 2: 1990-01-02; <bday-pk>
$   - gaba 3: 1990-01-03; <bday-pk>
```

To add a birthday:

```bash
bday-cli birthday add --name <name> --date <date>
$ Added:
$   - <name>: <date>; <bday-pk>
```

To remove a birthday:

```bash
bday-cli birthday remove --pk <bday-pk>
$ Removed!
```

## License

This project is under the [MIT License](https://opensource.org/licenses/MIT)
