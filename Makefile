.PHONY: clean test rustfmt clippy lint docs install

clean:
	cargo clean

build:
	cargo build

test:
	cargo test

rustfmt:
	cargo fmt --all -- --check

clippy:
	cargo clippy

lint: rustfmt clippy

ci: lint test

docs:
	cargo doc --no-deps --open

install:
	cargo build --release
	cp ./target/release/bday-cli ${HOME}/.local/bin

uninstall:
	rm ${HOME}/.local/bin/bday-cli
