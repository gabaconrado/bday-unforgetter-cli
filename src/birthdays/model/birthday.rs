/// Represents a birthday in the system
#[derive(Clone)]
pub struct Birthday {
    /// Birthday identifier
    pk: String,
    /// User owner of this birthday entry
    user_pk: String,
    /// Name of the birthday person
    name: String,
    /// The birthday date
    date: String,
}

// Public Birthday API
impl Birthday {
    /// Creates a new Birthday object from its parameters
    pub fn new(pk: &str, user_pk: &str, name: &str, date: &str) -> Self {
        Self {
            pk: pk.to_string(),
            user_pk: user_pk.to_string(),
            name: name.to_string(),
            date: date.to_string(),
        }
    }

    /// Returns the birthday identifier
    pub fn pk(&self) -> &str {
        &self.pk
    }

    /// Returns the birthday owner identifier
    pub fn user_pk(&self) -> &str {
        &self.user_pk
    }

    /// Returns the birthday person name
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Returns the birthday date
    pub fn date(&self) -> &str {
        &self.date
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::*;

    type Sut = Birthday;

    mod new {
        use super::*;

        #[test]
        fn new() {
            let (pk, user_pk, name, date) = ("0", "0", "gaba", "1990-01-01");
            let bday = Sut::new(pk, user_pk, name, date);

            assert_eq!(bday.pk, pk);
            assert_eq!(bday.user_pk, user_pk);
            assert_eq!(bday.name, name);
            assert_eq!(bday.date, date);
        }
    }

    mod getters {

        use super::*;

        #[fixture]
        fn bday() -> Sut {
            let (pk, user_pk, name, date) = ("0", "0", "gaba", "1990-01-01");
            Sut::new(pk, user_pk, name, date)
        }

        #[rstest]
        fn pk(bday: Sut) {
            assert_eq!(bday.pk(), "0");
        }

        #[rstest]
        fn user_pk(bday: Sut) {
            assert_eq!(bday.user_pk(), "0");
        }

        #[rstest]
        fn name(bday: Sut) {
            assert_eq!(bday.name(), "gaba");
        }

        #[rstest]
        fn date(bday: Sut) {
            assert_eq!(bday.date(), "1990-01-01");
        }
    }
}
