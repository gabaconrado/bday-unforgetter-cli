/// Birthday Api definition
pub trait BirthdayApi {
    type Output;

    fn add(&mut self, name: &str, date: &str) -> Self::Output;

    fn get(&self) -> Self::Output;

    fn remove(&mut self, pk: &str) -> Self::Output;
}
