/// Add a new birthday
pub trait AddBirthdayUC {
    type Output;
    type Error;

    fn add(&mut self, user_pk: &str, name: &str, date: &str) -> Result<Self::Output, Self::Error>;
}

/// Gets all birthdays
pub trait GetBirthdaysUC {
    type Error;
    type Output;

    fn get(&self, user_pk: &str) -> Result<Vec<Self::Output>, Self::Error>;
}

/// Remove a birthday
pub trait RemoveBirthdaysUC {
    type Error;

    fn remove(&mut self, pk: &str) -> Result<(), Self::Error>;
}
