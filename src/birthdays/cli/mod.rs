/// Text CLI API implementation
mod text;

pub use self::text::TextCliApi;
