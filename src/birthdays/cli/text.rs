use crate::birthdays::model::Birthday;
use crate::birthdays::traits::{AddBirthdayUC, BirthdayApi, GetBirthdaysUC, RemoveBirthdaysUC};

/// Basic Text birthday API object
pub struct TextCliApi<T>
where
    T: AddBirthdayUC + GetBirthdaysUC + RemoveBirthdaysUC,
{
    /// The user identifier
    user_pk: String,
    /// The data access object
    repository: T,
}

// Public API implementation
impl<T> TextCliApi<T>
where
    T: AddBirthdayUC + GetBirthdaysUC + RemoveBirthdaysUC,
{
    /// Creates a new TextCLI from a data access and the user pk
    pub fn new(repository: T, user_pk: &str) -> Self {
        let user_pk = String::from(user_pk);
        Self {
            repository,
            user_pk,
        }
    }

    /// Returns a shared reference to the internal repository object
    pub fn repository(&self) -> &T {
        &self.repository
    }

    /// Returns a mutable reference to the internal repository object
    pub fn repository_mut(&mut self) -> &mut T {
        &mut self.repository
    }
}

// Traits implementation
impl<T> BirthdayApi for TextCliApi<T>
where
    T: AddBirthdayUC<Output = Birthday> + GetBirthdaysUC<Output = Birthday> + RemoveBirthdaysUC,
    <T as AddBirthdayUC>::Error: std::fmt::Display,
    <T as GetBirthdaysUC>::Error: std::fmt::Display,
    <T as RemoveBirthdaysUC>::Error: std::fmt::Display,
{
    type Output = String;

    fn add(&mut self, name: &str, date: &str) -> Self::Output {
        match self.repository.add(&self.user_pk, name, date) {
            Ok(bday) => self.build_add_output(&bday),
            Err(err) => format!("Error adding a birthday: {}", err),
        }
    }

    fn get(&self) -> Self::Output {
        match self.repository.get(&self.user_pk) {
            Ok(bdays) => self.build_get_output(&bdays),
            Err(err) => format!("Error getting birthdays: {}", err),
        }
    }

    fn remove(&mut self, pk: &str) -> Self::Output {
        match self.repository.remove(pk) {
            Ok(_) => self.build_remove_output(),
            Err(err) => format!("Error removing a birthday: {}", err),
        }
    }
}

// Private implementation
impl<T> TextCliApi<T>
where
    T: AddBirthdayUC + GetBirthdaysUC + RemoveBirthdaysUC,
{
    /// Builds a string from a birthday object
    fn bday_to_str(bday: &Birthday) -> String {
        format!("\t- {}: {}; {}\n", bday.name(), bday.date(), bday.pk())
    }

    /// Builds the output for the add feature
    fn build_add_output(&self, bday: &Birthday) -> String {
        format!("Added:\n{}", Self::bday_to_str(bday))
    }

    /// Builds the output for the get feature
    fn build_get_output(&self, bdays: &[Birthday]) -> String {
        let bdays_str = bdays
            .iter()
            .map(|b| Self::bday_to_str(b))
            .collect::<Vec<String>>()
            .iter()
            .fold("".to_string(), |acc, b| acc + b);
        format!("Birthdays:\n{}", bdays_str)
    }

    /// Builds the output for the remove feature
    fn build_remove_output(&self) -> String {
        "Removed!".to_string()
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use rstest::*;

    type Tut = MockRepository;
    type Sut = TextCliApi<Tut>;

    #[fixture]
    fn cli() -> Sut {
        let repository = MockRepository(false);
        Sut::new(repository, "0")
    }

    mod new {
        use super::*;

        #[test]
        fn new() {
            let repository = MockRepository(false);
            let user_pk = "0";

            let cli = TextCliApi::new(repository, user_pk);

            assert_eq!(cli.user_pk, user_pk);
        }
    }

    mod add {
        use super::*;

        #[rstest]
        fn add(mut cli: Sut) {
            let (name, date) = ("gaba", "1990-01-01");
            let expected = "Added:\n\t- gaba: 1990-01-01; 0\n";

            let bday = cli.add(name, date);

            assert_eq!(bday, expected);
        }

        #[rstest]
        fn repository_error(mut cli: Sut) {
            cli.repository.0 = true;
            let (name, date) = ("gaba", "1990-01-01");
            let expected = "Error adding a birthday: generic error";

            let result = cli.add(name, date);

            assert_eq!(result, expected);
        }
    }

    mod get {
        use super::*;

        #[rstest]
        fn get(cli: Sut) {
            let expected = "Birthdays:\n\t- gaba: 1990-01-01; 0\n";

            let bdays = cli.get();

            assert_eq!(bdays, expected);
        }

        #[rstest]
        fn repository_error(mut cli: Sut) {
            cli.repository.0 = true;
            let expected = "Error getting birthdays: generic error";

            let result = cli.get();

            assert_eq!(result, expected);
        }
    }

    mod remove {
        use super::*;

        #[rstest]
        fn remove(mut cli: Sut) {
            let pk = "0";
            let expected = "Removed!";

            let bdays = cli.remove(pk);

            assert_eq!(bdays, expected);
        }

        #[rstest]
        fn repository_error(mut cli: Sut) {
            cli.repository.0 = true;
            let pk = "0";
            let expected = "Error removing a birthday: generic error";

            let result = cli.remove(pk);

            assert_eq!(result, expected);
        }
    }

    struct MockRepository(bool);
    struct MockRepositoryError;
    impl AddBirthdayUC for MockRepository {
        type Error = MockRepositoryError;
        type Output = Birthday;
        fn add(
            &mut self,
            user_pk: &str,
            name: &str,
            date: &str,
        ) -> Result<Self::Output, Self::Error> {
            if !self.0 {
                Ok(Birthday::new("0", user_pk, name, date))
            } else {
                Err(MockRepositoryError)
            }
        }
    }
    impl GetBirthdaysUC for MockRepository {
        type Error = MockRepositoryError;
        type Output = Birthday;
        fn get(&self, user_pk: &str) -> Result<Vec<Self::Output>, Self::Error> {
            if !self.0 {
                Ok(vec![Birthday::new("0", user_pk, "gaba", "1990-01-01")])
            } else {
                Err(MockRepositoryError)
            }
        }
    }
    impl RemoveBirthdaysUC for MockRepository {
        type Error = MockRepositoryError;
        fn remove(&mut self, _: &str) -> Result<(), Self::Error> {
            if !self.0 {
                Ok(())
            } else {
                Err(MockRepositoryError)
            }
        }
    }
    impl std::fmt::Display for MockRepositoryError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "generic error")
        }
    }
}
