/// In memory database implementation
mod database;

/// Error definitions
mod error;

// Exposed items
pub use self::database::MemoryRepository;
pub use self::error::MemoryRepositoryError;
