use std::fs::{File, OpenOptions};
use std::io::{Read, Write};

use crate::birthdays::model::Birthday;
use crate::birthdays::traits::{AddBirthdayUC, GetBirthdaysUC, RemoveBirthdaysUC};

use super::MemoryRepositoryError;

type MemoryRepositoryResult<T> = Result<T, MemoryRepositoryError>;

/// In memory non-persistent birthday repository object
#[derive(Default)]
pub struct MemoryRepository {
    birthdays: Vec<Birthday>,
}

impl MemoryRepository {
    /// Creates a new empty instance of the database
    pub fn new() -> Self {
        Self {
            birthdays: Vec::new(),
        }
    }

    /// Saves the current state of the db into the running directory
    ///
    /// The registers will be written as strings, one line per register
    pub fn save(&self, filepath: &str) -> MemoryRepositoryResult<()> {
        let mut db = Self::get_db_file_for_save(filepath)?;
        for bday in &self.birthdays {
            let line = format!("{}\n", Birthday::serialize(bday));
            db.write_all(line.as_bytes())?;
        }
        Ok(())
    }

    /// Loads all saved birthdays in the db file, if it exists
    pub fn load(&mut self, filepath: &str) -> MemoryRepositoryResult<()> {
        let mut db = Self::get_db_file_for_load(filepath)?;
        let mut content = String::new();
        db.read_to_string(&mut content)?;
        for line in content.lines() {
            let bday = Birthday::deserialize(line);
            self.birthdays.push(bday);
        }
        Ok(())
    }
}

// Traits implementations
impl AddBirthdayUC for MemoryRepository {
    type Error = MemoryRepositoryError;
    type Output = Birthday;

    fn add(&mut self, user_pk: &str, name: &str, date: &str) -> Result<Self::Output, Self::Error> {
        let pk = self.birthdays.len().to_string();
        let bday = Birthday::new(&pk, user_pk, name, date);
        self.birthdays.push(bday.clone());
        Ok(bday)
    }
}

// Private implementations
impl MemoryRepository {
    fn get_db_file_for_save(filepath: &str) -> MemoryRepositoryResult<File> {
        Ok(OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(filepath)?)
    }

    fn get_db_file_for_load(filepath: &str) -> MemoryRepositoryResult<File> {
        Ok(OpenOptions::new()
            .create(true)
            .write(true)
            .read(true)
            .open(filepath)?)
    }
}

impl Birthday {
    fn serialize(bday: &Birthday) -> String {
        format!(
            "{}|{}|{}|{}",
            bday.pk(),
            bday.user_pk(),
            bday.name(),
            bday.date()
        )
    }

    fn deserialize(bday: &str) -> Birthday {
        let components = bday.split('|').collect::<Vec<&str>>();
        Birthday::new(components[0], components[1], components[2], components[3])
    }
}

impl GetBirthdaysUC for MemoryRepository {
    type Error = MemoryRepositoryError;
    type Output = Birthday;

    fn get(&self, user_pk: &str) -> Result<Vec<Self::Output>, Self::Error> {
        Ok(self
            .birthdays
            .iter()
            .cloned()
            .filter(|bday| bday.user_pk() == user_pk)
            .collect())
    }
}

impl RemoveBirthdaysUC for MemoryRepository {
    type Error = MemoryRepositoryError;

    fn remove(&mut self, pk: &str) -> Result<(), Self::Error> {
        self.birthdays = self
            .birthdays
            .iter()
            .cloned()
            .filter(|bday| bday.pk() != pk)
            .collect();
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::*;

    type Sut = MemoryRepository;
    type SutResult<T> = Result<T, MemoryRepositoryError>;

    #[fixture]
    fn repository() -> Sut {
        Sut::new()
    }

    #[fixture]
    fn birthday() -> Birthday {
        Birthday::new("0", "0", "gaba", "1990-01-01")
    }

    #[fixture]
    fn birthday_serialized() -> String {
        "0|0|gaba|1990-01-01".to_string()
    }

    mod new {
        use super::*;

        #[test]
        fn new() {
            let db = MemoryRepository::new();
            assert_eq!(db.birthdays.len(), 0);
        }
    }

    mod add {
        use super::*;

        #[rstest]
        fn add(mut repository: Sut) -> SutResult<()> {
            let (pk, user_pk, name, date) = ("0", "0", "gaba", "1990-01-01");

            repository.add(user_pk, name, date)?;

            assert_eq!(repository.birthdays.len(), 1);
            let bday = &repository.birthdays[0];
            assert_eq!(bday.pk(), pk);
            assert_eq!(bday.name(), name);
            assert_eq!(bday.date(), date);
            Ok(())
        }
    }

    mod get {
        use super::*;

        #[rstest]
        fn get(mut repository: Sut, birthday: Birthday) -> SutResult<()> {
            repository.birthdays.push(birthday);

            let bdays = repository.get("0")?;
            assert_eq!(bdays.len(), 1);

            let bday = &bdays[0];
            assert_eq!(bday.pk(), "0");
            Ok(())
        }

        #[rstest]
        fn empty(repository: Sut) -> SutResult<()> {
            let bdays = repository.get("0")?;
            assert_eq!(bdays.len(), 0);
            Ok(())
        }
    }

    mod remove {
        use super::*;

        #[rstest]
        fn remove(mut repository: Sut, birthday: Birthday) -> SutResult<()> {
            repository.birthdays.push(birthday);

            repository.remove("0")?;

            assert_eq!(repository.birthdays.len(), 0);
            Ok(())
        }
    }

    mod serialize {
        use super::*;

        #[rstest]
        fn serialize(birthday: Birthday) {
            let serialized = Birthday::serialize(&birthday);

            assert_eq!(serialized, "0|0|gaba|1990-01-01".to_string());
        }
    }

    mod deserialize {
        use super::*;

        #[rstest]
        fn deserialize(birthday_serialized: String) {
            let bday = Birthday::deserialize(&birthday_serialized);

            assert_eq!(bday.pk(), "0");
            assert_eq!(bday.user_pk(), "0");
            assert_eq!(bday.name(), "gaba");
            assert_eq!(bday.date(), "1990-01-01");
        }
    }

    mod save {
        use super::*;

        #[rstest]
        fn save(mut repository: Sut, birthday: Birthday) -> Result<(), MemoryRepositoryError> {
            repository.add(birthday.user_pk(), birthday.name(), birthday.date())?;
            let filepath = "/tmp/bday-cli-test-save";
            let expected = format!(
                "{}|{}|{}|{}\n",
                birthday.pk(),
                birthday.user_pk(),
                birthday.name(),
                birthday.date()
            );

            repository.save(filepath)?;

            let content = String::from_utf8(std::fs::read(filepath)?).unwrap();
            assert_eq!(content, expected);
            std::fs::remove_file(filepath)?;
            Ok(())
        }

        #[rstest]
        fn disk_operation_failure(repository: Sut) {
            let filepath = "/bday-cli-test-save-strange-name/";

            let result = repository.save(filepath);

            assert!(matches!(
                result,
                Err(MemoryRepositoryError::DiskOperationFailure(_))
            ));
        }
    }

    mod load {
        use super::*;

        #[rstest]
        fn load(mut repository: Sut, birthday: Birthday) -> Result<(), MemoryRepositoryError> {
            let filepath = "/tmp/bday-cli-test-load";
            let line = format!("{}\n", Birthday::serialize(&birthday));
            std::fs::write(filepath, line.as_bytes())?;

            repository.load(filepath)?;

            assert_eq!(repository.birthdays.len(), 1);
            let bday = &repository.birthdays[0];
            assert_eq!(bday.pk(), birthday.pk());

            std::fs::remove_file(filepath)?;
            Ok(())
        }

        #[rstest]
        fn disk_operation_failure(mut repository: Sut) {
            let filepath = "/bday-cli-test-load-strange-name/";

            let result = repository.load(filepath);

            assert!(matches!(
                result,
                Err(MemoryRepositoryError::DiskOperationFailure(_))
            ));
        }
    }
}
