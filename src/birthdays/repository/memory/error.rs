use std::io;

type DiskErrorCause = io::Error;

/// The memory database will never error out, this is a placeholder
#[derive(Debug)]
pub enum MemoryRepositoryError {
    DiskOperationFailure(DiskErrorCause),
}

impl std::fmt::Display for MemoryRepositoryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MemoryRepositoryError::DiskOperationFailure(cause) => {
                write!(f, "Error in the memory repository: {}", cause)
            }
        }
    }
}

impl From<DiskErrorCause> for MemoryRepositoryError {
    fn from(err: std::io::Error) -> Self {
        MemoryRepositoryError::DiskOperationFailure(err)
    }
}
