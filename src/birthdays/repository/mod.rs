/// Birthday in-memory  non-persistent database
mod memory;

/// Birthday network API consumer
mod network;

pub use self::memory::{MemoryRepository, MemoryRepositoryError};
pub use self::network::{CurlAdapter, CurlAdapterError};
pub use self::network::{NetworkRepository, NetworkRepositoryError};
