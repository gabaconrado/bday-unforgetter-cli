pub enum HttpMethod {
    Get,
    Post,
    Delete,
}

pub trait NetworkAdapter {
    type Error;

    fn request(&self, method: HttpMethod, data: Option<String>) -> Result<String, Self::Error>;
}
