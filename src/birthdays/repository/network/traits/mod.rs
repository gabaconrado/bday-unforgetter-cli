/// Adapters interfaces
mod adapter;

pub use adapter::{HttpMethod, NetworkAdapter};
