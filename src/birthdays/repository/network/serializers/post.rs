use serde::{Deserialize, Serialize};
use serde_json;

use crate::birthdays::model::Birthday;

use super::Bday;
use super::SerializationError;

/// Object to represent a request to create a new birthday
#[derive(Serialize)]
pub struct PostRequest {
    user_pk: String,
    name: String,
    date: String,
}

/// Object to represent the response of a create birthday request
#[derive(Deserialize)]
pub struct PostResponse {
    birthday: Bday,
}

impl PostRequest {
    /// Creates a new request from its date
    pub fn new(user_pk: &str, name: &str, date: &str) -> Self {
        Self {
            user_pk: user_pk.to_string(),
            name: name.to_string(),
            date: date.to_string(),
        }
    }

    /// Serializes the request into a String
    pub fn serialize(&self) -> Result<String, SerializationError> {
        Ok(serde_json::to_string(self)?)
    }
}

impl PostResponse {
    /// Deserializes a response into the new birthday, fails if the parsing cannot be done
    pub fn deserialize(bday: &str) -> Result<Self, SerializationError> {
        Ok(serde_json::from_str(bday)?)
    }
}

impl From<PostResponse> for Birthday {
    fn from(response: PostResponse) -> Self {
        let bday = response.birthday;
        Birthday::from(&bday)
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use rstest::*;

    #[fixture]
    fn post_request() -> PostRequest {
        PostRequest::new("0", "gaba", "1990-01-01")
    }

    #[fixture]
    fn serialized_response() -> String {
        r#"{"birthday":{"pk":"0","user":"0","name":"gaba","date":"1990-01-01"}}"#.to_string()
    }

    mod request {
        use super::*;

        #[rstest]
        fn serialize(post_request: PostRequest) -> Result<(), SerializationError> {
            let serialized = post_request.serialize()?;

            assert_eq!(
                serialized,
                "{\"user_pk\":\"0\",\"name\":\"gaba\",\"date\":\"1990-01-01\"}"
            );
            Ok(())
        }
    }

    mod response {
        use super::*;

        #[rstest]
        fn deserialize(serialized_response: String) -> Result<(), SerializationError> {
            let response = PostResponse::deserialize(&serialized_response)?;

            let bday = response.birthday;
            assert_eq!(bday.pk, "0");
            assert_eq!(bday.user, "0");
            assert_eq!(bday.name, "gaba");
            assert_eq!(bday.date, "1990-01-01");
            Ok(())
        }

        #[test]
        fn into_birthday() {
            let birthday = Bday {
                pk: "0".to_string(),
                user: "0".to_string(),
                name: "gaba".to_string(),
                date: "1990-01-01".to_string(),
            };

            let response = PostResponse { birthday };

            let bday = Birthday::from(response);

            assert_eq!(bday.pk(), "0");
            assert_eq!(bday.user_pk(), "0");
            assert_eq!(bday.name(), "gaba");
            assert_eq!(bday.date(), "1990-01-01");
        }
    }
}
