use serde_json;

type FormatCause = serde_json::Error;

#[derive(Debug)]
pub enum SerializationError {
    ParsingError(FormatCause),
}

impl From<FormatCause> for SerializationError {
    fn from(err: FormatCause) -> Self {
        SerializationError::ParsingError(err)
    }
}

impl std::error::Error for SerializationError {}

impl std::fmt::Display for SerializationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SerializationError::ParsingError(err) => {
                write!(f, "Error serializing/deserializing: {}", err)
            }
        }
    }
}
