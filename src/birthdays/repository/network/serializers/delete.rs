use serde::Serialize;
use serde_json;

use super::SerializationError;

/// Object to represent a request to delete a birthday
#[derive(Serialize)]
pub struct DeleteRequest {
    pk: String,
}

impl DeleteRequest {
    /// Creates a new request object from a birthday pk
    pub fn new(bday_pk: &str) -> Self {
        Self {
            pk: bday_pk.to_string(),
        }
    }

    /// Serializes the request into a String
    pub fn serialize(&self) -> Result<String, SerializationError> {
        Ok(serde_json::to_string(self)?)
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use rstest::*;

    type Sut = DeleteRequest;

    #[fixture]
    fn request() -> Sut {
        let bday_pk = "0";
        Sut::new(bday_pk)
    }

    mod new {
        use super::*;

        #[test]
        fn new() {
            let bday_pk = "0";
            Sut::new(bday_pk);
        }
    }

    mod serialize {
        use super::*;

        #[rstest]
        fn serialize(request: Sut) -> Result<(), SerializationError> {
            let serialized = request.serialize()?;
            assert_eq!(serialized, "{\"pk\":\"0\"}");
            Ok(())
        }
    }
}
