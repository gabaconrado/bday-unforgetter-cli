use crate::birthdays::model::Birthday;
use serde::Deserialize;

/// Delete request serializers
mod delete;
/// Serialization/Deserialization related errors
mod error;
/// Get request serializers
mod get;
/// Post request serializers
mod post;

pub use delete::DeleteRequest;
pub use error::SerializationError;
pub use get::GetResponse;
pub use post::{PostRequest, PostResponse};

/// Internal structure to represent a birthday
#[derive(Deserialize)]
struct Bday {
    pk: String,
    user: String,
    name: String,
    date: String,
}

impl From<&Bday> for Birthday {
    fn from(bday: &Bday) -> Self {
        Birthday::new(&bday.pk, &bday.user, &bday.name, &bday.date)
    }
}
