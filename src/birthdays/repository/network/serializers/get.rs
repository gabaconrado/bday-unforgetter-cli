use serde::Deserialize;
use serde_json;

use crate::birthdays::model::Birthday;

use super::Bday;
use super::SerializationError;

/// Object to represent the response of a get request
#[derive(Deserialize)]
pub struct GetResponse {
    birthdays: Vec<Bday>,
}

impl GetResponse {
    /// Creates a GetResponse object from a serialized String, returns an error if
    /// the parsing fails
    pub fn deserialize(response: &str) -> Result<Self, SerializationError> {
        Ok(serde_json::from_str(response)?)
    }

    /// Returns the parsed birthdays as a vector
    pub fn birthdays(&self) -> Vec<Birthday> {
        self.birthdays.iter().map(|b| b.into()).collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::*;

    type Sut = GetResponse;

    #[fixture]
    fn response() -> String {
        r#"{"birthdays":[{"pk":"0","user":"0","name":"gaba","date":"1990-01-01"}]}"#.to_string()
    }

    #[fixture]
    fn wrong_response() -> String {
        "wrong".to_string()
    }

    mod deserialize {
        use super::*;

        #[rstest]
        fn deserialize(response: String) -> Result<(), SerializationError> {
            let deserialized = Sut::deserialize(&response)?;

            assert_eq!(deserialized.birthdays().len(), 1);
            assert_eq!(deserialized.birthdays()[0].pk(), "0");
            Ok(())
        }

        #[rstest]
        fn parsing_error(wrong_response: String) {
            let result = Sut::deserialize(&wrong_response);

            assert!(matches!(result, Err(SerializationError::ParsingError(_))))
        }
    }
}
