/// CURL adapter implementation
mod curl;
/// Network repository implementation
mod database;
/// Error definitions for the network repository
mod error;
/// Http Request/Response serializers/deserializers
mod serializers;
/// Types and interfaces for the network repository
mod traits;

pub use curl::{CurlAdapter, CurlAdapterError};
pub use database::NetworkRepository;
pub use error::NetworkRepositoryError;
use serializers::{DeleteRequest, GetResponse, PostRequest, PostResponse, SerializationError};
pub use traits::{HttpMethod, NetworkAdapter};
