use crate::birthdays::model::Birthday;
use crate::birthdays::traits::{AddBirthdayUC, GetBirthdaysUC, RemoveBirthdaysUC};

use super::HttpMethod;
use super::NetworkAdapter;
use super::NetworkRepositoryError;
use super::{DeleteRequest, GetResponse, PostRequest, PostResponse};

/// Birthday network fetcher repository object. Will consume the Birthday Unforgetter
/// public API with the help of a network adapter.
pub struct NetworkRepository<A>
where
    A: NetworkAdapter,
    NetworkRepositoryError: From<<A as NetworkAdapter>::Error>,
{
    adapter: A,
}

impl<A> NetworkRepository<A>
where
    A: NetworkAdapter,
    NetworkRepositoryError: From<<A as NetworkAdapter>::Error>,
{
    /// Creates a new instance of the repository from an adapter
    pub fn new(adapter: A) -> Self {
        Self { adapter }
    }
}

impl<A> AddBirthdayUC for NetworkRepository<A>
where
    A: NetworkAdapter,
    NetworkRepositoryError: From<<A as NetworkAdapter>::Error>,
{
    type Output = Birthday;
    type Error = NetworkRepositoryError;

    fn add(&mut self, user_pk: &str, name: &str, date: &str) -> Result<Self::Output, Self::Error> {
        let request = PostRequest::new(user_pk, name, date);
        let data = Some(request.serialize()?);
        let response = self.adapter.request(HttpMethod::Post, data)?;
        let response = PostResponse::deserialize(&response)?;
        Ok(Birthday::from(response))
    }
}

impl<A> GetBirthdaysUC for NetworkRepository<A>
where
    A: NetworkAdapter,
    NetworkRepositoryError: From<<A as NetworkAdapter>::Error>,
{
    type Output = Birthday;
    type Error = NetworkRepositoryError;

    fn get(&self, _: &str) -> Result<Vec<Self::Output>, Self::Error> {
        let response = self.adapter.request(HttpMethod::Get, None)?;
        let response = GetResponse::deserialize(&response)?;
        Ok(response.birthdays())
    }
}

impl<A> RemoveBirthdaysUC for NetworkRepository<A>
where
    A: NetworkAdapter,
    NetworkRepositoryError: From<<A as NetworkAdapter>::Error>,
{
    type Error = NetworkRepositoryError;

    fn remove(&mut self, pk: &str) -> Result<(), Self::Error> {
        let request = DeleteRequest::new(pk);
        let data = Some(request.serialize()?);
        self.adapter.request(HttpMethod::Delete, data)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::*;

    type Sut = NetworkRepository<MockAdapter>;

    #[fixture]
    fn adapter() -> MockAdapter {
        MockAdapter(false, false)
    }

    #[fixture]
    fn adapter_err() -> MockAdapter {
        MockAdapter(true, false)
    }

    #[fixture]
    fn adapter_wrong_response() -> MockAdapter {
        MockAdapter(false, true)
    }

    #[fixture]
    fn repository(adapter: MockAdapter) -> Sut {
        Sut::new(adapter)
    }

    #[fixture]
    fn repository_err(adapter_err: MockAdapter) -> Sut {
        Sut::new(adapter_err)
    }

    #[fixture]
    fn repository_wrong_response(adapter_wrong_response: MockAdapter) -> Sut {
        Sut::new(adapter_wrong_response)
    }

    mod new {
        use super::*;

        #[rstest]
        fn new(adapter: MockAdapter) {
            Sut::new(adapter);
        }
    }

    mod add {
        use super::*;

        #[rstest]
        fn add(mut repository: Sut) -> Result<(), NetworkRepositoryError> {
            let (user_pk, name, date) = ("0", "gaba", "1990-01-01");

            let user = repository.add(user_pk, name, date)?;

            assert_eq!(user.pk(), "0");
            assert_eq!(user.user_pk(), "0");
            assert_eq!(user.name(), "gaba");
            assert_eq!(user.date(), "1990-01-01");
            Ok(())
        }

        #[rstest]
        fn adapter_error(mut repository_err: Sut) {
            let (user_pk, name, date) = ("0", "gaba", "1990-01-01");

            let result = repository_err.add(user_pk, name, date);

            assert!(matches!(result, Err(NetworkRepositoryError::NetworkError)))
        }

        #[rstest]
        fn wrong_response(mut repository_wrong_response: Sut) {
            let (user_pk, name, date) = ("0", "gaba", "1990-01-01");

            let result = repository_wrong_response.add(user_pk, name, date);

            assert!(matches!(
                result,
                Err(NetworkRepositoryError::ParsingError(_))
            ))
        }
    }

    mod get {
        use super::*;

        #[rstest]
        fn get(repository: Sut) -> Result<(), NetworkRepositoryError> {
            let users = repository.get("nop")?;

            assert_eq!(users.len(), 1);

            let user = &users[0];
            assert_eq!(user.pk(), "0");
            assert_eq!(user.user_pk(), "0");
            assert_eq!(user.name(), "gaba");
            assert_eq!(user.date(), "1990-01-01");
            Ok(())
        }

        #[rstest]
        fn adapter_error(repository_err: Sut) {
            let result = repository_err.get("nop");

            assert!(matches!(result, Err(NetworkRepositoryError::NetworkError)))
        }

        #[rstest]
        fn wrong_response(repository_wrong_response: Sut) {
            let result = repository_wrong_response.get("nop");

            assert!(matches!(
                result,
                Err(NetworkRepositoryError::ParsingError(_))
            ))
        }
    }

    mod remove {
        use super::*;

        #[rstest]
        fn delete(mut repository: Sut) -> Result<(), NetworkRepositoryError> {
            let pk = "0";

            repository.remove(pk)?;

            Ok(())
        }

        #[rstest]
        fn adapter_error(mut repository_err: Sut) {
            let pk = "0";

            let result = repository_err.remove(pk);

            assert!(matches!(result, Err(NetworkRepositoryError::NetworkError)))
        }
    }

    struct MockAdapter(bool, bool);
    struct MockAdapterError;

    impl NetworkAdapter for MockAdapter {
        type Error = MockAdapterError;

        fn request(
            &self,
            method: HttpMethod,
            _data: Option<String>,
        ) -> Result<String, Self::Error> {
            if self.0 {
                return Err(MockAdapterError);
            }
            if self.1 {
                return Ok("wrong-response".to_string());
            }
            let response;
            let birthday = r#"{"pk":"0","user":"0","name":"gaba","date":"1990-01-01"}"#;
            match method {
                HttpMethod::Get => response = format!("{{\"birthdays\":[{}]}}", birthday),
                HttpMethod::Post => response = format!("{{\"birthday\":{}}}", birthday),
                HttpMethod::Delete => response = "{{}}".to_string(),
            }
            Ok(response)
        }
    }

    impl From<MockAdapterError> for NetworkRepositoryError {
        fn from(_: MockAdapterError) -> Self {
            Self::NetworkError
        }
    }
}
