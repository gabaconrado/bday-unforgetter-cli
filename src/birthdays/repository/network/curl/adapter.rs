use std::process::Command;

use crate::birthdays::repository::network::HttpMethod;

use super::super::NetworkAdapter;
use super::CurlAdapterError;

/// Object responsible for performing HTTP requests through `curl`
pub struct CurlAdapter {
    user_pk: String,
    api_key: String,
    bday_api_url: String,
}

// Public API implementation
impl CurlAdapter {
    /// Creates a new adapter from an user identifier and an api key
    pub fn new(user_pk: &str, api_key: &str, bday_api_url: &str) -> Self {
        Self {
            user_pk: user_pk.to_string(),
            api_key: api_key.to_string(),
            bday_api_url: bday_api_url.to_string(),
        }
    }
}

impl NetworkAdapter for CurlAdapter {
    type Error = CurlAdapterError;

    fn request(&self, method: HttpMethod, data: Option<String>) -> Result<String, Self::Error> {
        let http_method = match method {
            HttpMethod::Get => "GET".to_string(),
            HttpMethod::Post => "POST".to_string(),
            HttpMethod::Delete => "DELETE".to_string(),
        };
        self.call_curl(http_method, data)
    }
}

// Private functions implementation
impl CurlAdapter {
    fn call_curl(&self, method: String, data: Option<String>) -> Result<String, CurlAdapterError> {
        // Create command and add default args
        let mut command = Command::new("curl");
        command
            .arg("--http1.1")
            .args(&["-X", &method])
            .args(&["-H", &format!("User:{}", self.user_pk)])
            .args(&["-H", &format!("x-api-key:{}", self.api_key)])
            .args(&["-H", "Content-Type:application/json"]);
        // Add data if necessary
        if let Some(body) = data {
            command.args(&["-d", &body]);
        }

        // Get output and return
        let command_output = command.arg(&self.bday_api_url).output().unwrap();
        if command_output.status.success() {
            Ok(String::from_utf8(command_output.stdout).unwrap())
        } else {
            Err(CurlAdapterError::CmdError(command_output.status))
        }
    }
}
