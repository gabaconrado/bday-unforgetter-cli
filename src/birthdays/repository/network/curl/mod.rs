/// Implementation of the Network Adapter
mod adapter;
/// Curl adapter error definitions
mod error;

pub use self::adapter::CurlAdapter;
pub use self::error::CurlAdapterError;
