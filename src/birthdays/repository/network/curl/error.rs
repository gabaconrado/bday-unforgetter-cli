use super::super::NetworkRepositoryError;

type CmdCause = std::process::ExitStatus;

#[derive(Debug)]
pub enum CurlAdapterError {
    CmdError(CmdCause),
}

impl From<CurlAdapterError> for NetworkRepositoryError {
    fn from(_: CurlAdapterError) -> Self {
        NetworkRepositoryError::NetworkError
    }
}

impl std::error::Error for CurlAdapterError {}

impl std::fmt::Display for CurlAdapterError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CurlAdapterError::CmdError(err) => write!(f, "Error running command: {}", err),
        }
    }
}
