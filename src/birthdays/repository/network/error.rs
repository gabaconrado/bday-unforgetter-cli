use super::SerializationError;

type SerializationCause = SerializationError;

#[derive(Debug)]
pub enum NetworkRepositoryError {
    NetworkError,
    ParsingError(SerializationCause),
}

impl From<SerializationCause> for NetworkRepositoryError {
    fn from(err: SerializationCause) -> Self {
        Self::ParsingError(err)
    }
}

impl std::error::Error for NetworkRepositoryError {}

impl std::fmt::Display for NetworkRepositoryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            NetworkRepositoryError::NetworkError => write!(f, "Error fetching data from the API"),
            NetworkRepositoryError::ParsingError(err) => {
                write!(f, "Error parsing API response: {}", err)
            }
        }
    }
}
