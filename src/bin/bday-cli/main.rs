use bday_unforgetter_cli::birthdays::cli::TextCliApi;
use bday_unforgetter_cli::birthdays::repository::{CurlAdapter, NetworkRepository};
use bday_unforgetter_cli::birthdays::BirthdayApi;

mod arguments;
mod environment;
mod error;

use arguments::{BirthdayCmd, Module};
use environment::Environment;
use error::BdayCliError;

type Adapter = CurlAdapter;
type Repository = NetworkRepository<Adapter>;
type Api = TextCliApi<Repository>;

fn main() -> Result<(), BdayCliError> {
    let opts = arguments::parse();
    let environment = Environment::new()?;
    let repository = create_repository(&environment);
    let mut api = create_api(repository, &environment);
    match opts.module {
        Module::Birthday(cmd) => match cmd {
            BirthdayCmd::Add(args) => println!("{}", api.add(&args.name, &args.date)),
            BirthdayCmd::Get(_) => println!("{}", api.get()),
            BirthdayCmd::Remove(args) => println!("{}", api.remove(&args.pk)),
        },
    }
    Ok(())
}

fn create_repository(environment: &Environment) -> Repository {
    let adapter = CurlAdapter::new(
        &environment.user_pk,
        &environment.api_key,
        &environment.api_url,
    );

    NetworkRepository::new(adapter)
}

fn create_api(repository: Repository, environment: &Environment) -> Api {
    TextCliApi::new(repository, &environment.user_pk)
}
