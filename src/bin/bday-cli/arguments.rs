use clap::{AppSettings, Clap};

#[derive(Clap)]
#[clap(version = "0.1.0", author = "Gabriel Conrado <gabaconrado@gmail.com>")]
#[clap(setting = AppSettings::ColoredHelp)]
pub struct Opts {
    #[clap(subcommand, about = "Module to operate")]
    pub module: Module,
}

#[derive(Clap)]
pub enum Module {
    #[clap(subcommand, about = "Birthday related operations")]
    Birthday(BirthdayCmd),
}

#[derive(Clap)]
pub enum BirthdayCmd {
    #[clap(about = "Add a new birthday")]
    Add(AddCmd),
    #[clap(about = "Get all birthdays")]
    Get(GetCmd),
    #[clap(about = "Remove a saved birthday")]
    Remove(RemoveCmd),
}

#[derive(Clap)]
pub struct AddCmd {
    #[clap(short, long, about = "Birthday person name")]
    pub name: String,
    #[clap(short, long, about = "Birthday date")]
    pub date: String,
}

#[derive(Clap)]
pub struct GetCmd;

#[derive(Clap)]
pub struct RemoveCmd {
    #[clap(short, long, about = "Birthday identifier")]
    pub pk: String,
}

pub fn parse() -> Opts {
    Opts::parse()
}
