type MissingConfigurationCause = super::environment::EnvironmentError;
type RepositoryCause = bday_unforgetter_cli::birthdays::repository::MemoryRepositoryError;

#[derive(Debug)]
pub enum BdayCliError {
    MissingConfiguration(MissingConfigurationCause),
    RepositoryFailure(RepositoryCause),
}

impl std::error::Error for BdayCliError {}

impl std::fmt::Display for BdayCliError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            BdayCliError::MissingConfiguration(cause) => {
                write!(f, "Error configuring the CLI: {}", cause)
            }
            BdayCliError::RepositoryFailure(cause) => {
                write!(f, "Error during a repository operation: {}", cause)
            }
        }
    }
}

impl From<MissingConfigurationCause> for BdayCliError {
    fn from(err: MissingConfigurationCause) -> Self {
        BdayCliError::MissingConfiguration(err)
    }
}

impl From<RepositoryCause> for BdayCliError {
    fn from(err: RepositoryCause) -> Self {
        BdayCliError::RepositoryFailure(err)
    }
}
