use std::env;

const USER_PK_KEY: &str = "BDAY_CLI_USER_PK";
const BDAY_API_KEY_KEY: &str = "BDAY_CLI_API_KEY";
const BDAY_API_URL_KEY: &str = "BDAY_CLI_API_URL";

/// Structure to hold values that should live as environment variables
pub struct Environment {
    pub user_pk: String,
    pub api_key: String,
    pub api_url: String,
}

impl Environment {
    /// Create a new object, getting all necessary variables from the environment
    pub fn new() -> Result<Self, EnvironmentError> {
        let user_pk = Self::get_var(USER_PK_KEY)?;
        let api_key = Self::get_var(BDAY_API_KEY_KEY)?;
        let api_url = Self::get_var(BDAY_API_URL_KEY)?;
        Ok(Self {
            user_pk,
            api_key,
            api_url,
        })
    }

    fn get_var(key: &str) -> Result<String, EnvironmentError> {
        match env::var(key) {
            Ok(var) => Ok(var),
            Err(_) => Err(EnvironmentError::MissingVar(key.to_string())),
        }
    }
}

#[derive(Debug)]
pub enum EnvironmentError {
    MissingVar(String),
}

impl std::fmt::Display for EnvironmentError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            EnvironmentError::MissingVar(var) => write!(f, "Missing environment variable: {}", var),
        }
    }
}
